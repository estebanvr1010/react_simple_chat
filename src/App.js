import React from 'react';
import logo from './logo.svg';
import './App.css';
import FormLogin from './components/FormLogin';
import FormChat from './components/FormChat';
import FormSearch from './components/FormSearch';
/*
function App() {
  return (
    <FormLogin />
  );
}
*/

class App extends React.Component {


  render(){

    return (
      <div className="chatroom">
      <FormLogin />
      <FormChat />
      </div>
    );
  }


}


export default App;
