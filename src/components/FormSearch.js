import React, { Component } from 'react';

import YTSearch from 'youtube-api-v3-search';
import SearchBar from './search_bar';
import VideoList from './video_list';
import VideoDetail from './video_detail';
const API_KEY = 'AIzaSyCT5YNj0WpEUrt_4K8b3GZ6NoBZTOImXMA';

export default class FormSearch extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('react js');
    }

    videoSearch(term) {
        YTSearch({ key: API_KEY, term: term }, videos => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            }); //Same as this.setState({ videos : videos })
        });
    }

    render() {
/*
        const videoSearch = _.debounce(term => {
            this.videoSearch(term);
        }, 300);
*/
        return (
            <div>
            </div>
        );
        
    }


}